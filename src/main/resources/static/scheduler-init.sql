CREATE DATABASE `schedule` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use `schedule`;
-- ----------------------------
-- Table structure for app_service
-- ----------------------------
DROP TABLE IF EXISTS `app_service`;
CREATE TABLE `app_service` (
  `id` varchar(36) NOT NULL COMMENT '应用id',
  `status` int(1) DEFAULT '1' COMMENT '应用状态 0-已删除，1-正常 默认正常',
  `app_code` varchar(50) DEFAULT NULL COMMENT '应用编码',
  `app_name` varchar(100) DEFAULT NULL COMMENT '应用中文名称',
  `description` varchar(255) DEFAULT NULL COMMENT '服务描述',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for schedule_task
-- ----------------------------
DROP TABLE IF EXISTS `schedule_task`;
CREATE TABLE `schedule_task` (
  `id` varchar(36) NOT NULL COMMENT '任务id',
  `app_id` varchar(36) DEFAULT NULL COMMENT '应用id',
  `name` varchar(50) DEFAULT NULL COMMENT '任务名称',
  `cron_expression` varchar(50) DEFAULT NULL COMMENT '执行时间表达式',
  `content` varchar(255) DEFAULT NULL COMMENT '任务内容，不同的类型自定义',
  `type` int(11) DEFAULT NULL COMMENT '任务类型,1-自定义任务，2-业务触发任务',
  `status` int(1) DEFAULT '1' COMMENT '任务状态，1-启用  默认 启用状态，2-成功，3-失败,4-未启用',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建人',
  `description` varchar(255) DEFAULT NULL COMMENT '任务描述，执行需要参数的 为参数描述,客户端自定义解析格式',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for schedule_task_record
-- ----------------------------
DROP TABLE IF EXISTS `schedule_task_record`;
CREATE TABLE `schedule_task_record` (
  `id` varchar(36) NOT NULL COMMENT '任务执行记录id',
  `task_id` varchar(36) DEFAULT NULL COMMENT '任务id',
  `result` varchar(255) DEFAULT NULL COMMENT '执行结果',
  `err_msg` text COMMENT '错误信息',
  `duration` int(11) DEFAULT NULL COMMENT '执行时长',
  `start_time` timestamp NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` timestamp NULL DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
