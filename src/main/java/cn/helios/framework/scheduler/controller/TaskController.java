package cn.helios.framework.scheduler.controller;

import cn.helios.framework.scheduler.entity.ScheduleTask;
import cn.helios.framework.scheduler.service.SchedulerService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 14:17
 **/
@RestController
@RequestMapping("/task")
public class TaskController {

    @Resource
    private SchedulerService schedulerService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestBody ScheduleTask task) {
        schedulerService.addTask(task);
        return "SUCCESS";
    }

    @RequestMapping(value = "/modify", method = RequestMethod.POST)
    public String modify(@RequestBody ScheduleTask task) {
        schedulerService.modifyTask(task);
        return "SUCCESS";
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    public String del(@PathVariable("id") String id) {
        schedulerService.delTask(id);
        return "SUCCESS";
    }

}
