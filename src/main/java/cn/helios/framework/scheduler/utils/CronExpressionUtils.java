package cn.helios.framework.scheduler.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-12 19:46
 **/
public class CronExpressionUtils {

    private final static SimpleDateFormat format = new SimpleDateFormat("ss mm HH dd MM ? yyyy-yyyy");

    /**
     * 日起类型转换成执行表达式
     *
     * @param date 日期
     * @return
     */
    public static String date2cronExpression(Date date) {
        return format.format(date);
    }
}
