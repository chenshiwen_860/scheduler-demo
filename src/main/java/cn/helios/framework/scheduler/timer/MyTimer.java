package cn.helios.framework.scheduler.timer;

import cn.helios.framework.scheduler.entity.ScheduleTask;
import cn.helios.framework.scheduler.manager.SchedulerJobService;
import cn.helios.framework.scheduler.manager.TaskManager;
import cn.helios.framework.scheduler.utils.CronExpressionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 17:08
 **/
@Slf4j
@Component
public class MyTimer {

    @Resource
    private SchedulerJobService schedulerJobService;

    @PostConstruct
    public void init() {
        new Thread(() -> {
            for (int i = 0; i < 100000; i++) {
                Calendar instance = Calendar.getInstance();
                instance.add(Calendar.MINUTE, 1);
                Date date = instance.getTime();
                ScheduleTask task = new ScheduleTask();
                task.setId("task_" + (i + 1));
                TaskManager.set(task);
                schedulerJobService.addJob(UUID.randomUUID().toString().replaceAll("-", ""), CronExpressionUtils.date2cronExpression(date));
            }
        }).start();
    }

}
