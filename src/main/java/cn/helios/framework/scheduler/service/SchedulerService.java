package cn.helios.framework.scheduler.service;

import cn.helios.framework.scheduler.entity.ScheduleTask;

import java.util.UUID;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 13:45
 **/
public interface SchedulerService {

    /**
     * 添加任务
     *
     * @param task
     */
    void addTask(ScheduleTask task);

    /**
     * 删除任务
     *
     * @param id
     */
    void delTask(String id);

    /**
     * 修改任务
     *
     * @param task
     */
    void modifyTask(ScheduleTask task);

    default String getId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
