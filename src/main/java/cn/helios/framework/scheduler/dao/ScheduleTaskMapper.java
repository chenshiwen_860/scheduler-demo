package cn.helios.framework.scheduler.dao;

import cn.helios.framework.scheduler.entity.ScheduleTask;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-10 10:08
 **/
public interface ScheduleTaskMapper {

    /**
     * 根据任务id获取任务信息
     *
     * @param id 任务id
     * @return
     */
    ScheduleTask findById(@Param("id") String id);

    /**
     * 获取所有任务信息
     *
     * @return
     */
    List<ScheduleTask> findByStatus(@Param("status") Integer status);

    /**
     * 根据任务id删除任务信息
     *
     * @param id 任务id
     * @return
     */
    int deleteById(@Param("id") String id);

    /**
     * 保存任务信息
     *
     * @param task
     * @return
     */
    int save(ScheduleTask task);

    /**
     * 修改任务状态
     *
     * @param id     任务id
     * @param status 期望状态
     * @return
     */
    int modifyStatus(@Param("id") String id, @Param("status") Integer status, @Param("userName") String userName);

    /**
     * 修改任务信息
     *
     * @param task
     * @return
     */
    int modifyTask(@Param("task") ScheduleTask task);
}
