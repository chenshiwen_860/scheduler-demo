package cn.helios.framework.scheduler.dao;

import cn.helios.framework.scheduler.entity.ScheduleTaskRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-10 10:09
 **/
public interface ScheduleTaskRecordMapper {

    /**
     * 保存任务执行记录
     *
     * @param record 任务执行记录
     * @return
     */
    int save(ScheduleTaskRecord record);

    /**
     * 保存执行结果
     *
     * @param record 任务结果信息
     * @return
     */
    int updateForResult(@Param("record") ScheduleTaskRecord record);


    /**
     * 根据id获取任务执行记录信息
     *
     * @param id
     * @return
     */
    ScheduleTaskRecord findById(@Param("id") String id);


    /**
     * 根据taskId 获取执行记录信息
     *
     * @param taskId
     * @return
     */
    List<ScheduleTaskRecord> findByTaskId(@Param("taskId") String taskId);
}
