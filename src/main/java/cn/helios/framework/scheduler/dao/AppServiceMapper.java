package cn.helios.framework.scheduler.dao;

import cn.helios.framework.scheduler.entity.AppService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-10 10:09
 **/
public interface AppServiceMapper {

    /**
     * 根据状态查询应用信息
     *
     * @param status 状态
     * @return
     */
    List<AppService> findByStatus(@Param("status") Integer status);

    /**
     * 根据id删除应用信息
     *
     * @param id 应用id
     * @return
     */
    int delById(@Param("id") String id);

    /**
     * 保存
     *
     * @param appService 应用信息
     * @return
     */
    int save(AppService appService);
}
