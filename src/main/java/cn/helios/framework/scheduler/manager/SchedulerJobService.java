package cn.helios.framework.scheduler.manager;

import cn.helios.framework.scheduler.entity.ScheduleTask;

/**
 * task scheduler
 *
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 13:45
 **/
public interface SchedulerJobService {

    /**
     * 添加任务
     *
     * @param name
     * @param timer
     */
    void addJob(String name, String timer);

    /**
     * 修改任务
     *
     * @param name
     * @param timer
     */
    void modifyJob(String name, String timer);


    /**
     * 删除任务
     *
     * @param name
     */
    void deleteJob(String name);

    /**
     * 添加任务
     *
     * @param task
     */
    default void addTask(ScheduleTask task) {
        if (task == null)
            return;
        TaskManager.set(task);
        addJob(task.getId(), task.getCronExpression());
    }
}
