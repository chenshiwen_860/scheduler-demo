package cn.helios.framework.scheduler.manager;

import cn.helios.framework.scheduler.entity.ScheduleTask;

/**
 * 线程中任务管理
 * 解耦具体业务与任务
 *
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 13:58
 **/
public class TaskManager {

    private static final ThreadLocal<ScheduleTask> taskHolder = new ThreadLocal<>();

    /**
     * 设置当前线程任务
     *
     * @param task
     */
    public static void set(ScheduleTask task) {
        taskHolder.set(task);
    }

    /**
     * 获取线程中的任务
     *
     * @return
     */
    public static ScheduleTask get() {
        return taskHolder.get();
    }
}
