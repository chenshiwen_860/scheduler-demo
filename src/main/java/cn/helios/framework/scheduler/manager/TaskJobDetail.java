package cn.helios.framework.scheduler.manager;

import cn.helios.framework.scheduler.entity.ScheduleTask;
import lombok.Getter;
import lombok.Setter;
import org.quartz.impl.JobDetailImpl;

/**
 * 子定义任务信息
 *
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 13:56
 **/
@Setter
@Getter
public class TaskJobDetail extends JobDetailImpl {

    private ScheduleTask task;

}
