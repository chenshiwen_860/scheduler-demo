package cn.helios.framework.scheduler.manager;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-19 18:15
 **/
public interface QuartzConstant {

    /**
     * 触发器组
     */
    String TRIGGER_GROUP = "TRIGGER_GROUP";
    /**
     * 任务组
     */
    String JOB_GROUP = "JOB_GROUP";

}
