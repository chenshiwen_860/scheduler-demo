package cn.helios.framework.scheduler.manager;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 初始化调度器
 *
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 13:48
 **/
@Slf4j
@Component
public class SchedulerInit {

    @PostConstruct
    public void start() throws SchedulerException {
        log.info("scheduler starting ...");
        Scheduler scheduler = SchedulerManager.getScheduler();
        scheduler.start();
        log.info("scheduler started");
    }

}
