package cn.helios.framework.scheduler.manager;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * 调度器管理器
 *
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 13:49
 **/
public class SchedulerManager {

    private static final SchedulerFactory factory = new StdSchedulerFactory();

    /**
     * 获取调度器
     *
     * @return
     */
    public static Scheduler getScheduler() {
        try {
            return factory.getScheduler();
        } catch (SchedulerException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 添加任务
     *
     * @param jobDetail
     * @param trigger
     * @throws SchedulerException
     */
    public static void addJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException {
        factory.getScheduler().scheduleJob(jobDetail, trigger);
    }

}
