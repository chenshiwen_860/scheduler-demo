package cn.helios.framework.scheduler.manager;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.impl.triggers.CronTriggerImpl;

/**
 * 自定义任务执行方法
 *
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-20 10:37
 **/
@Slf4j
public class TaskJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        CronTriggerImpl trigger = (CronTriggerImpl) context.getTrigger();
        String cronExpression = trigger.getCronExpression();
        TaskJobDetail jobDetail = (TaskJobDetail) context.getJobDetail();
        log.info("执行表达式=====>[{}],时间----[{}],任务=====>[{}]", cronExpression, System.currentTimeMillis(), jobDetail.getTask());
    }

}
