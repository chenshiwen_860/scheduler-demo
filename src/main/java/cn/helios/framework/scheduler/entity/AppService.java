package cn.helios.framework.scheduler.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-10 09:53
 **/
@Setter
@Getter
public class AppService {

    @ApiModelProperty("应用id")
    private String id; // 应用id
    @ApiModelProperty("应用编码")
    private String appCode; // 应用编码
    @ApiModelProperty("应用名称")
    private String appName; // 应用名称
    @ApiModelProperty("应用描述")
    private String description; // 应用描述
    @ApiModelProperty("应用状态 0-已删除，1-正常")
    private Integer status; // 应用状态 0-已删除，1-正常
    @ApiModelProperty("创建时间")
    private Date createTime;

}
