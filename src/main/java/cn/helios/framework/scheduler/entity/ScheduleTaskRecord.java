package cn.helios.framework.scheduler.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-10 09:47
 **/
@Setter
@Getter
public class ScheduleTaskRecord implements Serializable {

    @ApiModelProperty("任务记录id")
    private String id; // 任务执行记录id
    @ApiModelProperty("任务id")
    private String taskId; // 任务id
    @ApiModelProperty("执行结果")
    private String result; // 执行结果
    @ApiModelProperty("错误信息")
    private String errMsg; // 错误信息
    @ApiModelProperty("执行耗时")
    private Long duration; // 执行耗时
    @ApiModelProperty("开始时间")
    private Date startTime; // 开始时间
    @ApiModelProperty("结束时间")
    private Date endTime; // 结束时间

}
