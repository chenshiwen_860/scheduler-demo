package cn.helios.framework.scheduler.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author chenshiwen@ibabygroup.cn
 * @date 2017-10-09 20:14
 **/
@Getter
@Setter
@ToString
public class ScheduleTask {
    @ApiModelProperty("任务id")
    private String id; // 任务id
    @ApiModelProperty("应用id")
    private String appId; // 所属应用id
    @ApiModelProperty("任务名称")
    private String name; // 任务名称
    @ApiModelProperty("执行周期")
    private String cronExpression; // 执行周围
    @ApiModelProperty("任务内容")
    private String content; // 任务内容
    @ApiModelProperty("任务类型")
    private Integer type; // 任务类型 1-通过url触发周期性，2-url触发一次性推送
    @ApiModelProperty("任务状态")
    private Integer status; // 任务状态，4-关闭，1-启用  默认 执行状态 一次性任务： 2-执行成功，3-执行失败
    @ApiModelProperty("任务描述")
    private String description; // 任务描述
    @ApiModelProperty("创建时间")
    private Date createTime; // 创建时间
    @ApiModelProperty("更新时间")
    private Date updateTime; // 更新时间
    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("所属服务")
    private AppService appService;


}
